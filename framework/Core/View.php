<?php

namespace framework\Core;

class view
{

    /**
     * @var \Piz\Router
     */
    protected $router;
    /**
     * @var swoole_http_server->request
     */
    protected $request;
    /**
     * @var swoole_http_server->response
     */
    protected $response;
    /**
     * @var swoole_server
     */
    protected $server;
    /**
     * @var swoole_server->task
     */
    protected $task;

    protected $tpl_var = array();
    protected $template_dir;
    protected $traceInfo = array();
    protected $model;
    protected $config;
    protected $tpl_file_type = '.html';

    function __construct()
    {
        $this->template_dir = \__CFG::ROOT . '/app/Templates/';

        if (!defined('TPL_PATH')) {
            define('TPL_PATH', $this->template_dir);
        }
    }

    public function json($array)
    {
        return json_encode($array);
    }


    function assign($key, $value)
    {
        // Verify $key is reliable.

        $this->tpl_var[$key] = $value;
    }

    /**
     * render template file, then display it.
     * @param string $_swoole_tpl_file
     */
    function display($_tpl_file = '')
    {
        if (empty($_tpl_file)) {
            //$_swoole_tpl_file = strtolower($this->swoole->env['mvc']['controller']) . '/' . strtolower($this->swoole->env['mvc']['view']) . '.php';
        }
        if (!is_file($this->template_dir . $_tpl_file . $this->tpl_file_type)) {
            //Error::info('template error', "template file[" . $this->template_dir . $_swoole_tpl_file . "] not found");
            echo 'template error,template file:' . $_tpl_file . $this->tpl_file_type;
        }
        extract($this->tpl_var);
        ob_start();
        include $this->template_dir . $_tpl_file . $this->tpl_file_type;
        $content = ob_get_contents();
        ob_end_clean();
        Http\Response->write($content);
    }

    public function __set($name, $object)
    {
        $this->$name = $object;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
