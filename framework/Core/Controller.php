<?php

namespace framework\Core;

class Controller
{

    protected $tpl_var = array();
    protected $template_dir;
    protected $traceInfo = array();
    protected $model;
    protected $config;
    protected $tpl_file_type = '.html';

    public function __construct()
    {
        $this->template_dir = ROOT_PATH . '/app/Templates/';

        if (!defined('TPL_PATH')) {
            define('TPL_PATH', $this->template_dir);
        }
    }

    function assign($key, $value)
    {
        // Verify $key is reliable.

        $this->tpl_var[$key] = $value;
    }

    /**
     * render template file, then display it.
     * @param string $_swoole_tpl_file
     */
    function display($_tpl_file = '')
    {
        if (empty($_tpl_file)) {
            //$_swoole_tpl_file = strtolower($this->swoole->env['mvc']['controller']) . '/' . strtolower($this->swoole->env['mvc']['view']) . '.php';
        }
        if (!is_file($this->template_dir . $_tpl_file . $this->tpl_file_type)) {
            //Error::info('template error', "template file[" . $this->template_dir . $_swoole_tpl_file . "] not found");
            echo 'template error,template file:' . $_tpl_file . $this->tpl_file_type;
        }
        extract($this->tpl_var);
        ob_start();
        include $this->template_dir . $_tpl_file . $this->tpl_file_type;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }


}