<?php

class autoload
{

    public static function get_loaded()
    {
        // 注册自动加载函数
        spl_autoload_register(function ($class) {
            $baseClasspath = \str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
            // 如果文件存在, 引用文件
            $classpath = dirname(__DIR__) . DIRECTORY_SEPARATOR . $baseClasspath;
            if (\is_file($classpath)) {
                require "{$classpath}";
                return;
            } else {
                return 'autoload fail,path :' . $classpath;
            }
        });
    }
}

return autoload::get_loaded();
