<?php

/**
 * version: v1
 * date:20190505
 * auth:chendongliang
 */
define('DEBUG', 'on');
define('WEBPATH', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);


require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'autoload.php';


$app = new framework\Core\Application();

/*
$http = new framework\Swoole\Server\Http("0.0.0.0", 9501);
$http->run();
*/


$http = new swoole_http_server('0.0.0.0', $app->config['http_port']);
$http->on("start", function ($server) use ($app) {
    echo "Swoole http server is started at http://127.0.0.1:" . $app->config['http_port'] . "\n";
});

$http->on("request", function ($request, $response) {
    $path_info = explode('/', $request->server['path_info']);
    if (empty($path_info)) {
        // 请求路径不合法, 设置为请求无效
        $response->status(400);
        $response->end("Invalid Path Info");
    }

    // 获取 模块, 控制器, 方法
    $model = (isset($path_info[1]) && !empty($path_info[1])) ? $path_info[1] : 'Home';
    $controller = (isset($path_info[2]) && !empty($path_info[2])) ? $path_info[2] : 'Index';
    $method = (isset($path_info[3]) && !empty($path_info[3])) ? $path_info[3] : 'index';

    try {
        $class_name = "\\app\\Controllers\\{$model}\\{$controller}";
        //Controllers\\Home\\Index

        $object = new $class_name();

        if (!method_exists($object, $method)) {
            // 请求方法不存在, 抛出异常
            throw new Exception("{$method} not found in {$class_name}");
        }

        $result = $object->$method($request, $response);

        $response->status(200);
        $response->end($result);
    } catch (Exception $e) {
        // 返回异常信息
        $response->status(503);
        $response->end(var_export($e, true));
    } catch (Error $e) {
        // 在 PHP7 中,可以增加这一句, 捕获更全面的异常
        $response->status(503);
        $response->end(var_export($e, true));
    }

    // $response->header("Content-Type", "text/plain");
    // $response->end("Hello World\n");
});


//工作进程启动时执行指定的代码
$http->on('WorkerStart', function ($serv, $workerId) {
    //var_dump(get_included_files()); //此数组中的文件表示进程启动前就加载了，所以无法reload
    //$serv->reload();
});


$http->start();
