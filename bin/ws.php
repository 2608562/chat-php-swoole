<?php

define('DEBUG', 'on');
define('WEBPATH', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'autoload.php';

$app = new framework\Core\Application();

$tableModel = new Swoole\Table(1024);
$tableModel->column('fd', swoole_table::TYPE_INT, 4);       //1,2,4,8
$tableModel->column('name', swoole_table::TYPE_STRING, 64);
$tableModel->column('age', swoole_table::TYPE_FLOAT);
$tableModel->column('token', swoole_table::TYPE_STRING, 32);
$tableModel->create();
$app->table['user'] = $tableModel;


$server = new Swoole\WebSocket\Server("0.0.0.0", $app->config['ws_port']);

$server->on('start', function ($server) use ($app) {
    echo 'ws start 0.0.0.0:' . $app->config['ws_port'] . "\n";
});

$server->on('open', function ($server, $request) use ($app) {
    echo "server: handshake success with fd{$request->fd}\n";
});

$server->on('message', function ($server, $frame) use ($app) {
    echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";

    $request = json_decode($frame->data, true);
    if ($request['ctl'] == 'login') {
        //登陆
        $client_name = $request['client_name'];

        if($token = $app->table['user']->get($frame->fd, 'token')){


        }else{
            $token = md5($client_name . time());
            $app->table['user']->set($frame->fd, ['fd' => $frame->fd, 'name' => $client_name, 'token' => $token]);
        }

        $return = [
            'ctl' => 'login',
            'client_name' => $client_name,
            'token' => $token
        ];



    } else if ($request['ctl'] == 'send') {
        //发送消息
        $return = [
            'ctl' => 'send',
            'status' => 'success'
        ];
    } else {
        $return = [
            'ctl' => 'undefined'
        ];
    }

    $server->push($frame->fd, json_encode($return));
});

$server->on('close', function ($server, $fd) {
    echo "client {$fd} closed\n";
});

$server->start();